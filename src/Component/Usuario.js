import React from "react";
import { Table, Button, ButtonGroup, Form, Row, Col } from "react-bootstrap";
import firebase from "firebase/app";
import "firebase/database";
import ReactHTMLTableToExcel from "react-html-table-to-excel";

class Usuario extends React.Component {
  constructor() {
    super();

    this.state = {
      id: "",
      nombre: "",
      apellido: "",
      correo: "",
      contrasena: "",

      show: false,
      busqueda: "",
      lista: [],
    };
  }

  cargarUsuarios = () => {
    // Carga todo los datos de padre reunion y los llena en lista
    this.setState({ lista: [] });
    firebase.database().ref("perfil/").off();
    firebase
      .database()
      .ref("perfil/")
      .on("child_added", (data) => {
        var listaTemp = this.state.lista;
        listaTemp.push(data);
        this.setState({ lista: listaTemp });
      });
  };

  componentDidMount() {
    this.cargarUsuarios();
  }

  handleClose = () => {
    this.setState({ show: false });
  };

  handlebusqueda = async (e) => {
    // obtiene la informacion de los campos de texto y los set en os estados
    await this.setState({ busqueda: e.target.value });
    this.filtroBusqueda();
  };

  remove = (id) => {
    firebase
      .database()
      .ref("perfil/" + id)
      .remove();
    this.cargarUsuarios();
  };

  filtroBusqueda = async () => {
    if (this.state.busqueda === "") {
      this.cargarUsuarios();
    } else {
      var data = await firebase
        .database()
        .ref("perfil/")
        .orderByChild("correo")
        .equalTo(this.state.busqueda)
        .once("value");

      if (data.val() == null) {
        // this.cargarUsuarios();
      } else {
        data.forEach((lista) => {
          if (lista.val().correo === this.state.busqueda) {
            var listaTemp = [];
            listaTemp.push(lista);
            this.setState({ lista: listaTemp });
          }
        });
      }
    }
  };

  render() {
    return (
      <div>
        <h1>Usuarios</h1>
        <Row>
          <Col xs={6} sm={6}>
            <Form.Control
              type="text"
              placeholder="Buscar"
              onChange={this.handlebusqueda}
            />
          </Col>

          <ButtonGroup className="mr-2 " aria-label="Segundo group">
            <ReactHTMLTableToExcel
              id="table"
              className="btn btn-success"
              table="tabla-usuario"
              filename="Usuario"
              sheet="usuario"
              buttonText=" Excel"
            />
          </ButtonGroup>
        </Row>

        <br />
        <Table
          responsive
          striped
          bordered
          hover
          variant="dark"
          id="tabla-usuario"
        >
          <thead>
            <tr>
              <th>Acciones</th>
              <th>Nombre</th>
              <th>Apellidos</th>
              <th>Correo</th>
            </tr>
          </thead>
          {this.state.lista.map((e) => {
            return (
              <tbody key={e.key}>
                <tr>
                  <td>
                    <ButtonGroup>
                      <Button
                        className=" btn btn-danger btn-sm"
                        onClick={() => this.remove(e.key)}
                      >
                        Eliminar
                      </Button>
                    </ButtonGroup>
                  </td>
                  <td>{e.val().nombre}</td>
                  <td>{e.val().apellido}</td>
                  <td>{e.val().correo}</td>
                </tr>
              </tbody>
            );
          })}
        </Table>
      </div>
    );
  }
}

export default Usuario;

import React from "react";
import Paper from "@material-ui/core/Paper";
import CssBaseline from "@material-ui/core/CssBaseline";
import Button from "@material-ui/core/Button";
import Styles from "../Utilitys/Styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import InputLabel from "@material-ui/core/InputLabel";
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "@material-ui/core/Select";
import firebase from "firebase/app";
import "firebase/database";
import { firebaseConfig } from "../Firebase";
import es from "date-fns/locale/es";
import { Alert } from "react-bootstrap";
import Copyright from "../Utilitys/Copyright";

registerLocale("es", es);

const validate = (values) => {
  const errors = {};

  if (
    !values.area ||
    !values.nombre ||
    !values.apellido ||
    !values.edad ||
    !values.identificacion ||
    !values.lider ||
    !values.contacto ||
    !values.visita ||
    !values.sintomas
  ) {
    errors.campos_obligatorios = "Todos los campos son obligatorios";
  }

  return errors;
};

class FormularioSabado extends React.Component {
  constructor() {
    super();

    this.state = {
      area: "",
      nombre: "",
      apellido: "",
      edad: "",
      identificacion: "",
      celular: "",
      fecha: "",
      lider: "",
      contacto: "",
      visita: "",
      sintomas: "",
      lista: [],
      errors: {},
      show: false,
    };
  }

  componentDidMount = () => {
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
    this.cargarlideres();
  };

  cargarlideres = () => {
    // realiza una consulta a la base de datos de padre lider y lo llena en el estado lista
    firebase
      .database()
      .ref("lider/")
      .on("child_added", (data) => {
        var listaTemp = this.state.lista;
        listaTemp.push(data);
        this.setState({ lista: listaTemp });
      });
  };

  atras = () => {
    window.location.href = "./";
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  handleShow = () => {
    this.setState({ show: true });
  };

  handleInput = async (e) => {
    // obtiene la informacion de los campos de texto y los set en os estados
    const { value, name } = e.target;
    await this.setState({ [name]: value });
  };
  handleFecha = async (e) => {
    await this.setState({ fecha: e });
  };

  limpiarsestados = () => {
    this.setState({ id: "" });
    this.setState({ area: "" });
    this.setState({ nombre: "" });
    this.setState({ apellido: "" });
    this.setState({ edad: "" });
    this.setState({ identificacion: "" });
    this.setState({ celular: "" });
    this.setState({ fecha: "" });
    this.setState({ lider: "" });
    this.setState({ contacto: "" });
    this.setState({ visita: "" });
    this.setState({ sintomas: "" });
  };

  filtroFecha = (date) => {
    const day = date.getDay();

    return (
      day !== 1 && day !== 2 && day !== 3 && day !== 4 && day !== 5 && day !== 0
    );
  };

  handleinsert = (e) => {
    var fecha = this.state.fecha;
    const { errors, ...sinErrors } = this.state;
    const result = validate(sinErrors);

    this.setState({ errors: result });
    if (!Object.keys(result).length) {
      var objeto = {
        area: "",
        nombre: "",
        apellido: "",
        edad: "",
        identificacion: "",
        celular: "",
        fecha: "",
        lider: "",
        contacto: "",
        visita: "",
        sintomas: "",
      };
      objeto.area = this.state.area;
      objeto.nombre = this.state.nombre;
      objeto.apellido = this.state.apellido;
      objeto.edad = this.state.edad;
      objeto.identificacion = this.state.identificacion;
      objeto.celular = this.state.celular;
      objeto.fecha = fecha.toLocaleDateString("es-ES");
      objeto.lider = JSON.parse(this.state.lider);
      objeto.contacto = this.state.contacto;
      objeto.visita = this.state.visita;
      objeto.sintomas = this.state.sintomas;

      firebase.database().ref("reunion-sabado/").push(objeto);

      this.handleShow();
      this.limpiarsestados();
    }
  };

  render() {
    const { errors } = this.state;
    return (
      <React.Fragment>
        <CssBaseline />
        <main className={this.props.classes.layoutCheckout}>
          <Paper className={this.props.classes.paperCheckout}>
            <Alert show={this.state.show} variant="success">
              <Alert.Heading>
                {" "}
                Gracias por realizar su Inscripción.
              </Alert.Heading>
              <p>
                Por favor siga todo los protocolos de bioseguridad que hemos
                preparado especialmente para ti. BIENVENIDO A CASA.
              </p>
            </Alert>
            <br></br>
            <br></br>
            <Typography component="h1" variant="h4" align="center">
              Formulario Reunión Sábado
            </Typography>

            <React.Fragment>
              <br></br>
              <React.Fragment>
                <Typography variant="h6" gutterBottom>
                  Información Personal
                </Typography>
                <Grid container spacing={3}>
                  <Grid item xs={12} md={12}>
                    <Select
                      required
                      id="area"
                      name="area"
                      label=" ¿Donde te diriges?"
                      fullWidth
                      autoComplete="cc-area"
                      onChange={this.handleInput}
                      value={this.state.area}
                    >
                      <MenuItem value="Auditorio">Auditorio</MenuItem>
                      <MenuItem value="Producción">Producción</MenuItem>
                      <MenuItem value="Alabaza">Alabaza</MenuItem>
                      <MenuItem value="Kids">Kids</MenuItem>
                      <MenuItem value="Servicio">Servicio</MenuItem>
                    </Select>
                    <FormHelperText>¿Donde te diriges?</FormHelperText>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      required
                      id="nombre"
                      name="nombre"
                      label="Nombre"
                      fullWidth
                      autoComplete="given-name"
                      onChange={this.handleInput}
                      value={this.state.nombre}
                      inputProps={{
                        maxLength: 20,
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      required
                      id="apellido"
                      name="apellido"
                      label="Apellidos"
                      fullWidth
                      autoComplete="family-apellido"
                      onChange={this.handleInput}
                      value={this.state.apellido}
                      inputProps={{
                        maxLength: 20,
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      required
                      id="edad"
                      name="edad"
                      label="Edad"
                      type="number"
                      fullWidth
                      autoComplete="family-edad"
                      onChange={this.handleInput}
                      value={this.state.edad}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      required
                      id="identificacion"
                      name="identificacion"
                      label="Identificación"
                      type="number"
                      fullWidth
                      autoComplete="family-identificacion"
                      onChange={this.handleInput}
                      value={this.state.identificacion}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      required
                      id="celular"
                      name="celular"
                      label="Celular"
                      fullWidth
                      autoComplete="Family-celular"
                      onChange={this.handleInput}
                      value={this.state.celular}
                      inputProps={{
                        maxLength: 15,
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <DatePicker
                      selected={this.state.fecha}
                      isClearable
                      locale="es"
                      dateFormat="dd 'de' MMMM 'de' yyyy"
                      filterDate={this.filtroFecha}
                      placeholderText="Seleccione fecha"
                      onChange={this.handleFecha}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <InputLabel htmlFor="lider">Líder</InputLabel>
                    <Select
                      required
                      label="Líder de 12"
                      name="lider"
                      fullWidth
                      onChange={this.handleInput}
                      value={this.state.lider}
                    >
                      {this.state.lista.map((e) => {
                        return (
                          <MenuItem key={e.key} value={JSON.stringify(e.val())}>
                            {e.val().nombre}
                          </MenuItem>
                        );
                      })}
                    </Select>

                    <FormHelperText>¿Quién es tu líder de 12?</FormHelperText>
                  </Grid>
                </Grid>
              </React.Fragment>
              <br></br>
              <br></br>
              <br></br>

              <React.Fragment>
                <Typography variant="h6" gutterBottom>
                  Información ante el Covid-19
                </Typography>
                <Grid container spacing={3}>
                  <Grid item xs={12} md={12}>
                    <Select
                      required
                      id="contacto"
                      name="contacto"
                      label="Has tenido contacto con personas que tengan covid-19?"
                      fullWidth
                      autoComplete="cc-contacto"
                      onChange={this.handleInput}
                      value={this.state.contacto}
                    >
                      <MenuItem value="Si">Si</MenuItem>
                      <MenuItem value="No">No</MenuItem>
                    </Select>
                    <FormHelperText>
                      ¿Has tenido contacto con personas que tengan covid-19?
                    </FormHelperText>
                  </Grid>

                  <Grid item xs={12} md={12}>
                    <Select
                      required
                      id="visita"
                      name="visita"
                      label="¿Has visitado una clínica en los últimos 8 días?"
                      fullWidth
                      autoComplete="cc-visita"
                      onChange={this.handleInput}
                      value={this.state.visita}
                    >
                      <MenuItem value="Si">Si</MenuItem>
                      <MenuItem value="No">No</MenuItem>
                    </Select>
                    <FormHelperText>
                      ¿Has visitado una clínica en los últimos 8 días?
                    </FormHelperText>
                  </Grid>

                  <Grid item xs={12} md={12}>
                    <Select
                      required
                      id="sintomas"
                      name="sintomas"
                      label="¿Has presentado síntomas gripales en las últimas 8 horas?"
                      fullWidth
                      autoComplete="cc-sintomas"
                      onChange={this.handleInput}
                      value={this.state.sintomas}
                    >
                      <MenuItem value="Si">Si</MenuItem>
                      <MenuItem value="No">No</MenuItem>
                    </Select>
                    <FormHelperText>
                      ¿Has presentado síntomas gripales en las últimas 8 horas?
                    </FormHelperText>
                  </Grid>
                </Grid>
              </React.Fragment>
              <br></br>
              {errors.campos_obligatorios && (
                <Alert variant="danger">{errors.campos_obligatorios} </Alert>
              )}

              <React.Fragment>
                <div className={this.props.classes.buttonsCheckout}>
                  <Button
                    onClick={this.atras}
                    className={this.props.classes.buttonCheckout}
                  >
                    Salir
                  </Button>

                  <Button
                    variant="contained"
                    color="primary"
                    onClick={this.handleinsert}
                    className={this.props.classes.buttonCheckout}
                  >
                    Enviar
                  </Button>
                </div>
                <br></br>
                <br></br>

                <Alert show={this.state.show} variant="success">
                  <Alert.Heading>
                    Gracias por realizar su inscripción.
                  </Alert.Heading>
                  <p>
                    Por favor siga todo los protocolos de bioseguridad que hemos
                    preparado especialmente para ti. BIENVENIDO A CASA.
                  </p>
                </Alert>
              </React.Fragment>
            </React.Fragment>
          </Paper>
        </main>
        <Copyright />
      </React.Fragment>
    );
  }
}

export default Styles(FormularioSabado);

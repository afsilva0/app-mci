import React from "react";
import Styles from "../Utilitys/Styles";
import firebase from "firebase/app";
import "firebase/database";
import { firebaseConfig } from "../Firebase";
import Navegador from "./Navegador";
import AlertDanger from "./AlertDanger";
import FormularioDomingo from "./FormularioDomingo";

class ValidarFormularioSabado extends React.Component {
  constructor() {
    super();

    this.state = {
      validar: 0,
    };
  }

  componentDidMount = () => {
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }

    this.verificar();
  };

  verificar = async () => {
    var fecha = new Date();

    var data = await firebase
      .database()
      .ref("configuracion-reunion/")
      .orderByChild("reunion")
      .equalTo("Domingo")
      .once("value");

    if (data.val() == null) {
      this.setState({ validar: 1 });
    } else {
      data.forEach((lista) => {
        if (
          fecha.getTime() >= lista.val().fechaInicioTime &&
          fecha.getTime() <= lista.val().fechaFinalTime
        ) {
          this.setState({ validar: 3 });
        } else {
          this.setState({ validar: 2 });
        }
      });
    }
  };

  validarformulario = (validar) => {
    switch (validar) {
      case 0:
        return;
      case 1:
        return (
          <AlertDanger
            title="Lo sentimos"
            body="El tiempo estimado para tu pre-registro se ha agotado. Mantente atento a las indicaciones de tu líder para el próximo registro."
          />
        );
      case 2:
        return (
          <AlertDanger
            title="Lo sentimos"
            body="El tiempo estimado para tu pre-registro se ha agotado. Mantente atento a las indicaciones de tu líder para el próximo registro."
          />
        );
      case 3:
        return <FormularioDomingo />;
      default:
        throw new Error("Unknown Reunion");
    }
  };

  atras = () => {
    window.location.href = "./";
  };

  render() {
    return (
      <React.Fragment>
        <Navegador />

        {this.validarformulario(this.state.validar)}
      </React.Fragment>
    );
  }
}

export default Styles(ValidarFormularioSabado);

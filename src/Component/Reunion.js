import React from "react";
import { Row, Col } from "react-bootstrap";
import Grid from "@material-ui/core/Grid";
import Styles from "../Utilitys/Styles";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import Select from "@material-ui/core/Select";
import ReunionSabado from "./ReunionSabado";
import ReunionDomingo from "./ReunionDomingo";

class Reunion extends React.Component {
  state = {
    reunion: 0,
  };

  mostrarreunion = (reunion) => {
    switch (reunion) {
      case 0:
        return <ReunionSabado />;
      case 1:
        return <ReunionDomingo />;

      default:
        throw new Error("Unknown Reunion");
    }
  };

  handleInput = async (e) => {
    const { value, name } = e.target;
    await this.setState({ [name]: value });
  };

  componentDidMount() {}

  render() {
    return (
      <div>
        <br></br>
        <Row>
          <Col xs={12} sm={12}>
            <Grid container spacing={3}>
              <Grid item xs={12} md={12}>
                <Select
                  required
                  id="reunion"
                  name="reunion"
                  label="Reunion?"
                  fullWidth
                  autoComplete="cc-reunion"
                  onChange={this.handleInput}
                  value={this.state.reunion}
                >
                  <MenuItem value={0}>Reunión Sábado</MenuItem>
                  <MenuItem value={1}>Reunión Domingo</MenuItem>
                </Select>
                <FormHelperText>Reunión</FormHelperText>
              </Grid>
            </Grid>
          </Col>

          <Col xs={0} sm={2}></Col>
        </Row>
        <br></br>
        <br></br>
        {this.mostrarreunion(this.state.reunion)}
      </div>
    );
  }
}

export default Styles(Reunion);

import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Toolbar from "@material-ui/core/Toolbar";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import Reunion from "./Reunion";
import Lider from "./Lider";
import Usuario from "./Usuario";
import ReactDOM from "react-dom";
import Cookies from "universal-cookie";
import { Modal, Button, ProgressBar, Image, Form, Card } from "react-bootstrap";
import MenuIcon from "@material-ui/icons/Menu";
import Hidden from "@material-ui/core/Hidden";
import ConfiguracionReunion from "./ConfiguracionReunion";

import firebase from "firebase/app";
import "firebase/database";
import "firebase/storage";
import "firebase/auth";
import { firebaseConfig } from "../Firebase";

const cookies = new Cookies();

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      bar: 0,
      permisos: true,
      uploadValue: 0,
      foto: "",
    };
  }

  componentDidMount() {
    ReactDOM.render(<Reunion />, document.getElementById("contendorData"));
    this.permisos();
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }

    firebase.auth().onAuthStateChanged((user) => {
      if (user != null) {
        this.setState({ id: user.uid });
        this.setState({ correo: user.email });
        this.setState({ foto: user.photoURL });
      } else {
        //no hay usuario logueado
      }
    });
  }

  permisos = () => {
    this.setState({ id: cookies.get("id") });

    if (cookies.get("datos_sesion")) {
      this.setState({ nombre: cookies.get("datos_sesion").nombre });
      this.setState({ apellido: cookies.get("datos_sesion").apellido });
      this.setState({ correo: cookies.get("datos_sesion").correo });
      this.setState({ foto: cookies.get("datos_sesion").foto });
    }
    if (cookies.get("permisos") !== "true") {
      this.setState({ permisos: false });
    }
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  iniciosesion = () => {
    document.location("/");
  };

  handleShow = () => {
    this.setState({ show: true });
  };
  handleClose = () => {
    this.setState({ show: false });
  };

  handleChangeFile(e) {
    const file = e.target.files[0];
    const fileRoute = e.target.value;
    const extpermitidas = /(.png|.jpg|.jpeg)$/i;

    if (!extpermitidas.exec(fileRoute)) {
      alert("No es una imagen");
    } else {
      const storageRef = firebase.storage().ref("imagenes/" + file.name);
      const task = storageRef.put(file);

      task.on(
        "state_changed",
        (snapshot) => {
          let percentage =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          this.setState({
            uploadValue: percentage,
          });
        },
        (error) => {
          console.error(error.message);
        },
        () => {
          storageRef.getDownloadURL().then((url) => {
            //alert(url);
            this.setState({ foto: url });
            this.updateFoto();
          });
        }
      );
    }
  }

  updateFoto = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user != null) {
        user
          .updateProfile({
            photoURL: this.state.foto,
          })
          .then(() => {
            // Update successful.
          })
          .catch((error) => {
            // An error happened.
          });
      } else {
        //no hay usuario logueado
      }
    });
  };

  cerrarSesion = () => {
    firebase
      .auth()
      .signOut()
      .then(() => {
        console.log("salio de la sesion");
      });
    cookies.remove("datos_sesion", { path: "/" });
    cookies.remove("permisos", { path: "/" });
    cookies.remove("id", { path: "/" });
    this.setState({ id: "" });
    this.setState({ nombre: "" });
    this.setState({ apellido: "" });
    this.setState({ correo: "" });
    this.setState({ foto: "" });
  };

  render() {
    return (
      <div>
        <React.Fragment>
          <AppBar
            component="div"
            color="primary"
            position="static"
            elevation={0}
          >
            <br></br>

            <Toolbar>
              <Grid container alignItems="center" spacing={1}>
                <Hidden smUp>
                  <Grid item>
                    <IconButton
                      color="inherit"
                      aria-label="open drawer"
                      onClick={this.props.onDrawerToggle}
                    >
                      <MenuIcon />
                    </IconButton>
                  </Grid>
                </Hidden>
                <Grid item xs></Grid>
                <Grid item></Grid>

                <Tooltip title="Cerrar Sesion">
                  <Button
                    className="outline-primary"
                    onClick={this.cerrarSesion}
                  >
                    Cerrar Sesión
                  </Button>
                </Tooltip>

                <Grid item>
                  <Tooltip title="Alerts • No alerts">
                    <IconButton color="inherit">
                      <NotificationsIcon />
                    </IconButton>
                  </Tooltip>
                </Grid>
                <Grid item>
                  <div>
                    <IconButton color="inherit" onClick={this.handleShow}>
                      <Avatar src={this.state.foto} alt="My Avatar" />
                    </IconButton>
                    <Modal show={this.state.show} onHide={this.handleClose}>
                      <Modal.Header closeButton>
                        <Modal.Title>Usuario</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <ProgressBar now={this.state.uploadValue} />
                        <br />
                        <Form.File
                          id="foto"
                          label="Foto"
                          onChange={this.handleChangeFile.bind(this)}
                        />
                        <br />

                        <Card bg="light" key="" text="dark" className="mb-2">
                          <Card.Body>
                            <Card.Title>Perfil </Card.Title>

                            <hr></hr>
                            <h6>Foto del perfil </h6>
                            <Image
                              style={{ margin: "auto", width: "60%" }}
                              src={this.state.foto}
                              alt="Foto"
                              rounded
                            />
                            <hr></hr>

                            <h6>Nombre: </h6>
                            <p>{this.state.nombre}</p>
                            <h6>Apellidos:</h6>
                            <p> {this.state.apellido}</p>
                            <h6>Correo:</h6>
                            <p> {this.state.correo}</p>
                          </Card.Body>
                        </Card>
                      </Modal.Body>
                      <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleClose}>
                          Cerrar
                        </Button>
                      </Modal.Footer>
                    </Modal>
                  </div>
                </Grid>
              </Grid>
            </Toolbar>
            <br></br>

            <Grid item xs>
              <Typography color="inherit" variant="h5">
                Menú
              </Typography>
            </Grid>
            <br></br>
          </AppBar>
          <AppBar color="primary" position="static" elevation={0}>
            <Tabs
              value={this.state.bar}
              textColor="inherit"
              variant="scrollable"
              scrollButtons="auto"
            >
              <Tab
                textColor="inherit"
                label="Reunión"
                title="reunion"
                onClick={() => {
                  this.setState({ bar: 0 });
                  ReactDOM.render(
                    <Reunion />,
                    document.getElementById("contendorData")
                  );
                }}
              ></Tab>
              <Tab
                hidden={this.state.permisos}
                textColor="inherit"
                label="Líder de 12 "
                title="lider"
                onClick={() => {
                  this.setState({ bar: 1 });
                  ReactDOM.render(
                    <Lider />,
                    document.getElementById("contendorData")
                  );
                }}
              ></Tab>

              <Tab
                hidden={this.state.permisos}
                textColor="inherit"
                label="Usuarios"
                title="usuarios"
                onClick={() => {
                  this.setState({ bar: 2 });
                  ReactDOM.render(
                    <Usuario />,
                    document.getElementById("contendorData")
                  );
                }}
              ></Tab>

              <Tab
                hidden={this.state.permisos}
                textColor="inherit"
                label="Configuración Reunión"
                title="configuracion"
                onClick={() => {
                  this.setState({ bar: 3 });
                  ReactDOM.render(
                    <ConfiguracionReunion />,
                    document.getElementById("contendorData")
                  );
                }}
              ></Tab>
            </Tabs>
          </AppBar>
        </React.Fragment>
      </div>
    );
  }
}

export default Header;

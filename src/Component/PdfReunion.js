import React from "react";

import {
  Page,
  Text,
  Document,
  StyleSheet,
  Image,
  View,
} from "@react-pdf/renderer";
import { PDFViewer } from "@react-pdf/renderer";
import firebase from "firebase/app";
import "firebase/database";
//import { firebaseConfig } from "../Firebase";

class PdfReunion extends React.Component {
  state = { lista: [] };

  cargarUsuarios = () => {
    // Carga todo los datos de padre reunion y los llena en lista
    this.setState({ lista: [] });
    firebase.database().ref("usuarios/").off();
    firebase
      .database()
      .ref("usuarios/")
      .on("child_added", (data) => {
        var listaTemp = this.state.lista;
        listaTemp.push(data);
        this.setState({ lista: listaTemp });
      });
  };

  componentDidMount() {
    /*if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
    this.cargarUsuarios();*/
  }
  render() {
    return (
      <div>
        <PDFViewer style={styles.vierwer}>
          <MyDocument />
        </PDFViewer>
      </div>
    );
  }
}

// Create styles
const styles = StyleSheet.create({
  vierwer: {
    width: "100%",
    height: "900px",
    margin: "0",
  },
  page: {
    backgroundColor: "#E4E4E4",
  },

  image: {
    marginVertical: 15,
    marginHorizontal: 100,
    objectFit: "cover",
  },
  pageNumber: {
    position: "absolute",
    fontSize: 12,
    bottom: 30,
    left: 0,
    right: 0,
    textAlign: "center",
    color: "grey",
  },

  text: {
    margin: 12,
    fontSize: 14,
    textAlign: "justify",
    paddingRight: "5%",
    paddingLeft: "5%",
  },
  subtitle: {
    fontSize: 18,
    margin: 12,
    paddingLeft: "5%",
  },
  author: {
    fontSize: 12,
    textAlign: "center",
    marginBottom: 20,
  },
  title: {
    fontSize: 24,
    textAlign: "center",
    paddingTop: "5%",
    paddingBottom: "2%",
  },
});

// Create Document Component
const MyDocument = () => (
  <Document>
    <Page style={styles.page}>
      <View>
        <Text style={styles.title}>MCI INTERNACIONAL </Text>

        <Text style={styles.author}>CÉSAR CASTELLANOS</Text>

        <Image
          style={styles.image}
          src="https://mci12.com/wp-content/uploads/2020/02/group-7.png"
        />
        <Text style={styles.subtitle}>Hombre de Visión</Text>
        <Text style={styles.text}>
          El Pastor César Castellanos es considerado como un hombre visionario.
          Cuando no tenía iglesia, Dios le habló y le dijo “Sueña con una
          iglesia muy grande, porque los sueños son el lenguaje de mi espíritu”
          Desde ese momento Dios le dió el don de la fe. Ha estado 3 veces
          clínicamente muerto, ha tenido encuentros muy poderosos con Jesús.
          Dios le ha dado mucha revelación a través del poder de “La Sangre de
          Jesús” donde ha visto milagros de sanidad sobrenaturales, y también
          milagros de grandes conquistas.
        </Text>
        <Text style={styles.text}>
          Es un hombre apasionado por las almas, desde el momento que se
          convirtió comenzó a predicar en las calles. Hoy en día a través de la
          “Visión G12” mentorea a mucho líderes y pastores y los lleva a soñar,
          a ser formado y a alcanzar su máximo potencial. El pastor César tiene
          una iglesia muy grande, pero lo más increíble es que no es una iglesia
          simplemente de miembro sino de discípulos y en su gran mayoría
          líderes. Vive en Bogotá junto a su esposa Claudia, es padre de 5 hijos
          y 11 nietos.
        </Text>

        <Text
          style={styles.pageNumber}
          render={({ pageNumber, totalPages }) =>
            `${pageNumber} / ${totalPages}`
          }
          fixed
        />
      </View>
    </Page>
  </Document>
);

export default PdfReunion;

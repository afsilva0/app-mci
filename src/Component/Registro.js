import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Copyright from "../Utilitys/Copyright";
import Styles from "../Utilitys/Styles";
import Container from "@material-ui/core/Container";
import firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";

import Navegador from "./Navegador";
import md5 from "md5";
import { Alert } from "react-bootstrap";
import Cookies from "universal-cookie";

const cookies = new Cookies();

const validate = (values) => {
  const errors = {};
  var contrasena_md5 = md5(values.admin);
  const re = /^([\da-z_-]+)@([\da-z-]+)\.([a-z]{2,6})$/;
  if (!re.exec(values.correo)) {
    errors.campos_obligatorios = "EL correo es incorrecto";
  }

  if (
    contrasena_md5 !== "343adb71f202e0537ab72c170b518eb2" &&
    contrasena_md5 !== "2618eff517f957c182a23c602fbaf2b4"
  ) {
    errors.campos_obligatorios = "El código del administrador es incorrecto";
  }
  if (
    !values.nombre ||
    !values.apellido ||
    !values.correo ||
    !values.contrasena ||
    !values.admin
  ) {
    errors.campos_obligatorios = "Todos los campos son obligatorios";
  }

  return errors;
};

class Registro extends React.Component {
  constructor() {
    super();
    this.state = {
      nombre: "",
      apellido: "",
      correo: "",
      contrasena: "",
      admin: "",
      errors: {},
      show: 0,
    };
  }

  iniciosesion = () => {
    window.location.href = "./iniciosesion";
  };
  handleInput = async (e) => {
    // obtiene la informacion de los campos de texto y los set en os estados
    const { value, name } = e.target;
    await this.setState({ [name]: value });
  };

  handleinsert = async (e) => {
    const { errors, ...sinErrors } = this.state;
    const result = validate(sinErrors);

    this.setState({ errors: result });

    //Esta linea de codigo valida si no existe errores y hace el registro
    if (!Object.keys(result).length) {
      firebase
        .auth()
        .createUserWithEmailAndPassword(
          this.state.correo,
          this.state.contrasena
        )
        .then((userCredential) => {
          console.log("sesion iniciada");
          this.crearPerfil();
        })
        .catch((error) => {
          // Algo salio mal.
          if (error.code === "auth/invalid-email") {
            // El email esta mal formado
            this.setState({ show: 3 });
          } else if (error.code === "auth/email-already-in-use") {
            // El email ya existe
            this.setState({ show: 2 });
          } else {
            console.log(error.code);
          }
        });
    }
  };

  crearPerfil = async () => {
    var objet = {
      nombre: "",
      apellido: "",
      correo: "",
      rol: "",
    };

    var contrasena_md5 = md5(this.state.admin);
    if (contrasena_md5 === "2618eff517f957c182a23c602fbaf2b4") {
      objet.nombre = this.state.nombre;
      objet.apellido = this.state.apellido;
      objet.correo = this.state.correo;

      objet.rol = "admin";
    } else {
      objet.nombre = this.state.nombre;
      objet.apellido = this.state.apellido;
      objet.correo = this.state.correo;
      objet.rol = "usuario";
    }
    firebase.database().ref("perfil/").push(objet);

    //luego de hacer el registro valida si se creo corectamente e ingresa sesion
    var data = await firebase
      .database()
      .ref("perfil/")
      .orderByChild("correo")
      .equalTo(this.state.correo)
      .once("value");
    if (data.val() == null) {
      this.setState({ show: 1 });
    } else {
      data.forEach((lista) => {
        var datos_Sesion = lista.val();
        cookies.set("id", lista.key, {
          path: "/",
        });
        cookies.set("datos_sesion", JSON.stringify(datos_Sesion), {
          path: "/",
        });
        if (lista.val().rol === "admin") {
          cookies.set("permisos", false, { path: "/" });
        } else {
          cookies.set("permisos", true, { path: "/" });
        }

        this.setState({ show: 4 });
        window.location.href = "./ingreso";
      });
    }
  };

  validarformulario = (validar) => {
    switch (validar) {
      case 0:
        return;
      case 1:
        return <Alert variant="danger">Ingreso inválido</Alert>;
      case 2:
        return (
          <Alert variant="danger"> Ya existe un usuario con ese correo </Alert>
        );
      case 3:
        return <Alert variant="danger"> EL correo es incorrecto </Alert>;
      case 4:
        return <Alert variant="success"> Bienvenido </Alert>;

      default:
        throw new Error("Unknown Reunion");
    }
  };

  render() {
    const { errors } = this.state;
    return (
      <div>
        <Navegador />
        <Container component="main" maxWidth="sm">
          <CssBaseline />
          <div className={this.props.classes.paper}>
            <Avatar className={this.props.classes.avatar}></Avatar>
            <Typography component="h1" variant="h5">
              Regístrate
            </Typography>
            <form className={this.props.classes.form} noValidate>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="fname"
                    name="nombre"
                    variant="outlined"
                    required
                    fullWidth
                    id="nombre"
                    label="Nombre"
                    autoFocus
                    onChange={this.handleInput}
                    inputProps={{
                      maxLength: 20,
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="apellido"
                    label="Apellidos"
                    name="apellido"
                    autoComplete="lname"
                    onChange={this.handleInput}
                    inputProps={{
                      maxLength: 20,
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="correo"
                    label="Correo Email"
                    name="correo"
                    autoComplete="email"
                    onChange={this.handleInput}
                    inputProps={{
                      maxLength: 30,
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    name="contrasena"
                    label="Contraseña"
                    type="password"
                    id="contrasena"
                    autoComplete="current-password"
                    onChange={this.handleInput}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    name="admin"
                    label="Ingrese el código del administrador"
                    type="password"
                    id="admin"
                    autoComplete="current-password"
                    onChange={this.handleInput}
                    inputProps={{
                      maxLength: 12,
                    }}
                  />
                </Grid>
              </Grid>
              <br></br>
              {errors.campos_obligatorios && (
                <Alert variant="danger">{errors.campos_obligatorios} </Alert>
              )}
              {this.validarformulario(this.state.show)}

              <Button
                type="Button"
                fullWidth
                variant="contained"
                color="primary"
                className={this.props.classes.submit}
                onClick={this.handleinsert}
              >
                Registrarse
              </Button>

              <Grid container justify="flex-end">
                <Grid item>
                  <Link onClick={this.iniciosesion} variant="body2">
                    ¿Ya tienes una cuenta? Inicia Sesión
                  </Link>
                </Grid>
              </Grid>
            </form>
          </div>
          <Box mt={5}>
            <Copyright />
          </Box>
        </Container>
      </div>
    );
  }
}

export default Styles(Registro);

import React from "react";
import Paper from "@material-ui/core/Paper";
import Styles from "../Utilitys/Styles";
import { Alert } from "react-bootstrap";
import Copyright from "../Utilitys/Copyright";

class AlertDanger extends React.Component {
  render() {
    return (
      <React.Fragment>
        <main className={this.props.classes.layoutCheckout}>
          <Paper className={this.props.classes.paperCheckout}>
            <Alert show={true} variant="danger">
              <Alert.Heading>{this.props.title}</Alert.Heading>
              <p>{this.props.body}</p>
            </Alert>
          </Paper>
        </main>
        <Copyright />
      </React.Fragment>
    );
  }
}

export default Styles(AlertDanger);

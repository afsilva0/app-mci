import React from "react";

import { Nav, Navbar, Form, ButtonGroup } from "react-bootstrap";
import ReactDOM from "react-dom";
import Iniciosesion from "./Iniciosesion";
import Registro from "./Registro";

import { Link } from "react-router-dom";

class Navegador extends React.Component {
  iniciosesion = () => {
    ReactDOM.render(
      <Iniciosesion></Iniciosesion>,
      document.getElementById("root")
    );
  };

  registro = () => {
    ReactDOM.render(<Registro></Registro>, document.getElementById("root"));
  };

  render() {
    return (
      <div>
        <Navbar bg="dark" variant="dark">
          <Link className="btn btn-dark" to="/">
            MCI Cali
          </Link>
          <Nav className="mr-auto"></Nav>
          <Form inline>
            <ButtonGroup className="mr-2 " aria-label="Primer group">
              <Link className="btn  btn-outline-light" to="/iniciosesion">
                Iniciar Sesión
              </Link>
            </ButtonGroup>
            <ButtonGroup className="mr-2 " aria-label="Segundo group">
              <Link className="btn btn-outline-danger" to="/registro">
                Registro
              </Link>
            </ButtonGroup>
          </Form>
        </Navbar>
      </div>
    );
  }
}

export default Navegador;

import React from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import Copyright from "../Utilitys/Copyright";
import Styles from "../Utilitys/Styles";
import Redes from "./Redes";
import Familiar from "../Imagenes/Familiar.jpg";
import Jovenes from "../Imagenes/Jovenes.jpg";
import Kids from "../Imagenes/Kids.jpg";
import PreJuveniles from "../Imagenes/PreJuveniles.jpg";
import { Card } from "react-bootstrap";

import Iframe from "react-iframe";
class Informacion extends React.Component {
  render() {
    return (
      <div>
        <Container>
          <br></br>
          <br></br>

          <Row>
            <Col xs={12} sm={6}>
              <Card bg="dark" key={1} text="white" className="mb-2">
                <Image
                  className={this.props.classes.imagenInformacion}
                  src={Familiar}
                  rounded
                />
                <Card.Header>REUNIÓN FAMILIAR</Card.Header>
                <Card.Body>
                  <Card.Title> Horarios </Card.Title>
                  <Card.Text>Domingos – 7:30 AM / 9:40 AM / 11:40 AM</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col xs={12} sm={6}>
              <Card bg="dark" key={1} text="white" className="mb-2">
                <Image
                  className={this.props.classes.imagenInformacion}
                  src={Jovenes}
                  rounded
                />
                <Card.Header>REUNIÓN JÓVENES</Card.Header>
                <Card.Body>
                  <Card.Title> Horarios </Card.Title>
                  <Card.Text>Sábados – 5:00 PM</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col xs={12} sm={6}>
              <Card bg="dark" key={1} text="white" className="mb-2">
                <Image
                  className={this.props.classes.imagenInformacion}
                  src={Kids}
                  rounded
                />
                <Card.Header> REUNIÓN KIDS </Card.Header>
                <Card.Body>
                  <Card.Title> Horarios </Card.Title>
                  <Card.Text>Domingos – 10:30 AM</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col xs={12} sm={6}>
              <Card bg="dark" key={1} text="white" className="mb-2">
                <Image
                  className={this.props.classes.imagenInformacion}
                  src={PreJuveniles}
                  rounded
                />
                <Card.Header> REUNIÓN PREJUVENILES </Card.Header>
                <Card.Body>
                  <Card.Title> Horarios </Card.Title>
                  <Card.Text>Sábados – 3:50 PM</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <br></br>
          <br></br>
        </Container>

        <br></br>
        <Row>
          <Col xs={0} sm={3}></Col>
          <Col xs={12} sm={6}>
            <div
              style={{
                position: "relative",
                paddingBottom: "10%",
                overflow: "hidden",
              }}
            >
              <Iframe
                style={{
                  position: "absolute",
                  width: "100%",
                  height: "100%",
                }}
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d248.90362759539164!2d-76.5001463349098!3d3.4807672987087943!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e30a7d505fcce3d%3A0x557bef144ddf8dea!2sG12%20Centro%20de%20Eventos!5e0!3m2!1ses!2sco!4v1603329837193!5m2!1ses!2sco"
                width="600"
                height="450"
                frameborder="0"
                allowfullscreen=""
                aria-hidden="false"
                tabindex="0"
              ></Iframe>
            </div>
          </Col>
          <Col xs={0} sm={3}></Col>
        </Row>
        <br></br>
        <Redes />
        <br></br>
        <Copyright />
        <br></br>
      </div>
    );
  }
}

export default Styles(Informacion);

import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Copyright from "../Utilitys/Copyright";
import Styles from "../Utilitys/Styles";
import Container from "@material-ui/core/Container";
import firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";
import { firebaseConfig } from "../Firebase";
import Navegador from "./Navegador";
import Cookies from "universal-cookie";
import { Alert } from "react-bootstrap";

const cookies = new Cookies();

const validate = (values) => {
  const errors = {};

  const re = /^([\da-z_-]+)@([\da-z-]+)\.([a-z]{2,6})$/;
  if (!re.exec(values.correo)) {
    errors.campos_obligatorios = "EL correo es incorrecto";
  } else {
  }

  if (!values.correo || !values.contrasena) {
    errors.campos_obligatorios = "Todos los campos son obligatorios";
  }

  return errors;
};

class Iniciosesion extends React.Component {
  constructor() {
    super();

    this.state = {
      correo: "",
      contrasena: "",
      history: "",
      errors: {},
      show: 0,
    };
  }

  handleInput = async (e) => {
    // obtiene la informacion de los campos de texto y los set en os estados
    const { value, name } = e.target;
    await this.setState({
      [name]: value,
    });
  };

  handleSubmit = async () => {
    const { errors, ...sinErrors } = this.state;
    const result = validate(sinErrors);

    this.setState({ errors: result });

    //Esta linea de codigo valida si no existe errores y hace el registro
    if (!Object.keys(result).length) {
      this.crearCook();
      await firebase
        .auth()
        .signInWithEmailAndPassword(this.state.correo, this.state.contrasena)
        .then((userCredential) => {
          console.log("sesion iniciada");
          // this.crearCook();
        })
        .catch((error) => {
          // Algo salio mal.
          if (error.code === "auth/invalid-email") {
            // El email esta mal formado
          } else if (error.code === "auth/wrong-password") {
            this.setState({ show: 2 });
          } else {
            console.log(error);
            alert("fff");
          }
        });
    }
  };

  crearCook = async () => {
    var data = await firebase
      .database()
      .ref("perfil/")
      .orderByChild("correo")
      .equalTo(this.state.correo)
      .once("value");
    //valida en la base de datos si existen usuario con el correo y con las contraseña digitado si es asi lo envia al ingreso
    if (data.val() == null) {
      this.setState({ show: 1 });
    } else {
      data.forEach((lista) => {
        alert("sjfsjf");
        cookies.set("id", lista.key, {
          path: "/",
        });
        cookies.set("datos_sesion", JSON.stringify(lista.val()), {
          path: "/",
        });
        //valida el rol del usuario
        if (lista.val().rol === "admin") {
          cookies.set("permisos", false, { path: "/" });
        } else {
          cookies.set("permisos", true, { path: "/" });
        }
        this.setState({ show: 3 });
      });
    }
  };

  validarformulario = (validar) => {
    switch (validar) {
      case 0:
        return;
      case 1:
        return <Alert variant="danger">Ingreso inválido</Alert>;
      case 2:
        return <Alert variant="danger"> Contraseña incorrecta</Alert>;
      case 3:
        return <Alert variant="success"> Bienvenido </Alert>;
      default:
        throw new Error("Unknown Reunion");
    }
  };

  componentDidMount() {
    //verifica si un usuario esta iniciado sesion y lo envia al ingreso
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
    firebase.auth().onAuthStateChanged((user) => {
      if (user != null) {
        window.location.href = "./ingreso";
      } else {
      }
    });
  }

  render() {
    const { errors } = this.state;
    return (
      <div>
        <Navegador />
        <Container component="main" maxWidth="sm">
          <CssBaseline />
          <div className={this.props.classes.paper}>
            <Avatar className={this.props.classes.avatar}></Avatar>
            <Typography component="h1" variant="h5">
              Iniciar Sesión
            </Typography>
            <div className={this.props.classes.form} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Correo Email"
                type="email"
                name="correo"
                autoComplete="email"
                autoFocus
                onChange={this.handleInput}
                inputProps={{
                  maxLength: 30,
                }}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="contrasena"
                label="Contraseña"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={this.handleInput}
                inputProps={{
                  maxLength: 20,
                }}
              />
              <br></br>
              {errors.campos_obligatorios && (
                <Alert variant="danger">{errors.campos_obligatorios} </Alert>
              )}
              {this.validarformulario(this.state.show)}
              <Button
                type="button"
                fullWidth
                variant="contained"
                color="primary"
                className={this.props.classes.submit}
                onClick={this.handleSubmit}
              >
                Iniciar Sesión
              </Button>
            </div>
          </div>
          <Box mt={8}>
            <Copyright />
          </Box>
        </Container>
      </div>
    );
  }
}

export default Styles(Iniciosesion);

import React from "react";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Styles from "../Utilitys/Styles";
import Informacion from "./Informacion";
import Navegador from "./Navegador";
import { ButtonGroup, Row, Col } from "react-bootstrap";

class Inicio extends React.Component {
  registrodomingo = () => {
    window.location.href = "./formulariodomingo";
  };

  registrosabado = () => {
    window.location.href = "./formulariosabado";
  };

  render() {
    return (
      <div>
        <Navegador />
        <div className={this.props.classes.background}>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <Typography
            color="inherit"
            align="center"
            variant="h3"
            marked="center"
            className={this.props.classes.textInicio}
          >
            BIENVENIDOS A MCI CALI
          </Typography>
          <br></br>
          <br></br>

          <Typography
            color="inherit"
            align="center"
            variant="h5"
            className={this.props.classes.h5Inicio}
          >
            Somos una Iglesia que busca seguir la gran comisión de Jesús: ganar
            almas y hacer discípulos. Nuestro deseo es que las personas se
            sientan como en familia cada vez que llegan a alguna de nuestras
            reuniones o a alguna de nuestras células.
          </Typography>

          <Row>
            <Col xs={0} sm={2}></Col>
            <Col xs={12} sm={4}>
              <ButtonGroup className="mr-2 mt-3">
                <Button
                  color="secondary"
                  variant="contained"
                  size="large"
                  className={this.props.classes.button}
                  onClick={this.registrosabado}
                >
                  Pre-registro Sábado
                </Button>
              </ButtonGroup>
            </Col>
            <Col xs={12} sm={4}>
              <ButtonGroup className="mr-2 mt-3">
                <Button
                  color="secondary"
                  variant="contained"
                  size="large"
                  className={this.props.classes.button}
                  onClick={this.registrodomingo}
                >
                  Pre-registro Domingo
                </Button>
              </ButtonGroup>
            </Col>
            <Col xs={0} sm={2}></Col>
          </Row>
          <br></br>
          <br></br>
          <Typography
            variant="body2"
            color="inherit"
            className={this.props.classes.more}
          >
            Descubre la experiencia
          </Typography>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <div className="App-header">
            <Informacion />
          </div>
        </div>
      </div>
    );
  }
}

export default Styles(Inicio);

import React from "react";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import YouTubeIcon from "@material-ui/icons/YouTube";
import IconButton from "@material-ui/core/IconButton";

class Redes extends React.Component {
  state = {};
  render() {
    return (
      <div>
        <IconButton color="inherit">
          <a
            href="https://www.facebook.com/MCI-Cali-205640222942581"
            rel="noopener"
          >
            <FacebookIcon color="primary" fontSize="large"></FacebookIcon>
          </a>
        </IconButton>

        <IconButton color="inherit" onClick={this.handleShow}>
          <a href="https://www.instagram.com/mci_cali/" rel="noopener">
            <InstagramIcon color="secondary" fontSize="large"></InstagramIcon>
          </a>
        </IconButton>

        <IconButton color="inherit" onClick={this.handleShow}>
          <a
            href="https://www.youtube.com/channel/UCnnc1xFTHkul0ksG8rL6vtA"
            rel="noopener"
          >
            <YouTubeIcon fontSize="large"></YouTubeIcon>
          </a>
        </IconButton>
      </div>
    );
  }
}

export default Redes;

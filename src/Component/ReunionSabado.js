import React from "react";
import {
  Table,
  Button,
  Form,
  Row,
  Col,
  ButtonGroup,
  Modal,
  Alert,
} from "react-bootstrap";
import firebase from "firebase/app";
import "firebase/database";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Styles from "../Utilitys/Styles";
import ReactHTMLTableToExcel from "react-html-table-to-excel";
import Typography from "@material-ui/core/Typography";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import InputLabel from "@material-ui/core/InputLabel";
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "@material-ui/core/Select";
import es from "date-fns/locale/es";

import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";

registerLocale("es", es);

const validate = (values) => {
  const errors = {};

  if (
    !values.area ||
    !values.nombre ||
    !values.apellido ||
    !values.edad ||
    !values.identificacion ||
    !values.celular ||
    !values.lider ||
    !values.contacto ||
    !values.visita ||
    !values.sintomas
  ) {
    errors.campos_obligatorios = "Todos los campos son obligatorios";
  }

  return errors;
};

const validate2 = (values) => {
  const errors2 = {};

  if (!values.temperatura || !values.pregunta_covid) {
    errors2.campos_obligatorios = "Todos los campos son obligatorios";
  }

  return errors2;
};

class ReunionSabado extends React.Component {
  state = {
    id: "",
    area: "",
    nombre: "",
    apellido: "",
    edad: "",
    identificacion: "",
    celular: "",
    fecha: "",
    lider: "",
    contacto: "",
    visita: "",
    sintomas: "",
    temperatura: "",
    pregunta_covid: "",
    lista: [],
    lista2: [],
    filtro: [],
    busqueda: "",
    errors: {},
    errors2: {},

    showModalRegistro: false,
    showModalActualizar: false,
  };

  handleInput = async (e) => {
    const { value, name } = e.target;
    await this.setState({ [name]: value });
  };

  handleFecha = async (e) => {
    await this.setState({ fecha: e });
  };

  llenar = (id) => {
    this.setState({ id: id });

    this.handleShow();
  };

  filtroFecha = (date) => {
    const day = date.getDay();

    return (
      day !== 1 && day !== 2 && day !== 3 && day !== 4 && day !== 5 && day !== 0
    );
  };

  limpiarsestados = () => {
    this.setState({ id: "" });
    this.setState({ area: "" });
    this.setState({ nombre: "" });
    this.setState({ apellido: "" });
    this.setState({ edad: "" });
    this.setState({ identificacion: "" });
    this.setState({ celular: "" });
    this.setState({ fecha: "" });
    this.setState({ lider: "" });
    this.setState({ contacto: "" });
    this.setState({ visita: "" });
    this.setState({ sintomas: "" });
    this.setState({ temperatura: "" });
    this.setState({ pregunta_covid: "" });
  };

  cargarReuniones = () => {
    // Carga todo los datos de padre reunion y los llena en lista
    this.setState({ lista: [] });
    firebase.database().ref("reunion-sabado/").off();
    firebase
      .database()
      .ref("reunion-sabado/")
      .on("child_added", (data) => {
        var listaTemp = this.state.lista;
        listaTemp.push(data);
        this.setState({ lista: listaTemp });
      });
  };

  cargarLideres = () => {
    // Carga todo los datos de padre lider y los llena en lista2
    this.setState({ lista2: [] });
    firebase.database().ref("lider/").off();
    firebase
      .database()
      .ref("lider/")
      .on("child_added", (data) => {
        var listaTemp = this.state.lista2;
        listaTemp.push(data);
        this.setState({ lista2: listaTemp });
      });
  };

  componentDidMount() {
    this.cargarReuniones();
    this.cargarLideres();
  }

  modalCloseRegistro = () => {
    this.setState({ showModalRegistro: false });
  };

  modalShowRegistro = () => {
    this.setState({ showModalRegistro: true });
  };

  modalCloseActualizar = () => {
    this.setState({ showModalActualizar: false });
  };
  modalShowActualizar = () => {
    this.setState({ showModalActualizar: true });
  };

  llenar = (id) => {
    this.setState({ id: id });

    this.modalShowActualizar();
  };

  handlebusqueda = async (e) => {
    // obtiene la informacion de los campos de texto y los set en os estados
    await this.setState({ busqueda: e.target.value });
    this.filtroBusqueda();
  };

  remove = (id) => {
    firebase
      .database()
      .ref("reunion-sabado/" + id)
      .remove();
    this.cargarReuniones();
  };

  pdf = () => {
    window.location.href = "/pdfReunion";
  };

  handleinsert = (e) => {
    var fecha = this.state.fecha;
    const { errors, ...sinErrors } = this.state;
    const result = validate(sinErrors);

    this.setState({ errors: result });
    //valida si no hay errores permite el registro

    if (!Object.keys(result).length) {
      var objeto = {
        area: "",
        nombre: "",
        apellido: "",
        edad: "",
        identificacion: "",
        celular: "",
        fecha: "",
        lider: "",
        contacto: "",
        visita: "",
        sintomas: "",
      };
      objeto.area = this.state.area;
      objeto.nombre = this.state.nombre;
      objeto.apellido = this.state.apellido;
      objeto.edad = this.state.edad;
      objeto.identificacion = this.state.identificacion;
      objeto.celular = this.state.celular;
      objeto.fecha = fecha.toLocaleDateString("es-ES");
      objeto.lider = JSON.parse(this.state.lider);
      objeto.contacto = this.state.contacto;
      objeto.visita = this.state.visita;
      objeto.sintomas = this.state.sintomas;

      firebase.database().ref("reunion-sabado/").push(objeto);
      var objeto = {
        area: "",
        nombre: "",
        apellido: "",
        edad: "",
        identificacion: "",
        celular: "",
        fecha: "",
        lider: "",
        contacto: "",
        visita: "",
        sintomas: "",
      };
      objeto.area = this.state.area;
      objeto.nombre = this.state.nombre;
      objeto.apellido = this.state.apellido;
      objeto.edad = this.state.edad;
      objeto.identificacion = this.state.identificacion;
      objeto.celular = this.state.celular;
      objeto.fecha = fecha.toLocaleDateString("es-ES");
      objeto.lider = JSON.parse(this.state.lider);
      objeto.contacto = this.state.contacto;
      objeto.visita = this.state.visita;
      objeto.sintomas = this.state.sintomas;

      firebase.database().ref("reunion-sabado/").push(objeto);
      this.limpiarsestados();
      this.modalCloseRegistro();
    }
  };

  handleupdate = () => {
    const { errors2, ...sinErrors2 } = this.state;
    const result2 = validate2(sinErrors2);

    this.setState({ errors2: result2 });
    //valida si no hay errores permite la atualizacion
    if (!Object.keys(result2).length) {
      var objeto = {
        temperatura: "",
        pregunta_covid: "",
      };
      objeto.temperatura = this.state.temperatura;
      objeto.pregunta_covid = this.state.pregunta_covid;

      firebase
        .database()
        .ref("reunion-sabado/" + this.state.id)
        .update(objeto);
      this.cargarReuniones();
      this.limpiarsestados();
      this.modalCloseActualizar();
    }
  };

  filtroBusqueda = async () => {
    if (this.state.busqueda === "") {
      this.cargarReuniones();
    } else {
      var data = await firebase
        .database()
        .ref("reunion-sabado/")
        .orderByChild("identificacion")
        .equalTo(this.state.busqueda)
        .once("value");

      if (data.val() == null) {
        // this.cargarReuniones();
      } else {
        data.forEach((lista) => {
          if (lista.val().identificacion === this.state.busqueda) {
            var listaTemp = [];
            listaTemp.push(lista);
            this.setState({ lista: listaTemp });
          }
        });
      }
    }
  };

  confirm = (id) => {
    confirmAlert({
      title: "Confirmación",
      message: "Está seguro que desea eliminar este registro.",
      buttons: [
        {
          label: "Si",
          onClick: () => this.remove(id),
        },
        {
          label: "No",
          onClick: "",
        },
      ],
    });
  };

  render() {
    const { errors } = this.state;
    const { errors2 } = this.state;
    return (
      <div>
        <h1>Reunión Sábado</h1>
        <br></br>

        <Row>
          <Col xs={6} sm={6}>
            <Form.Control
              type="text"
              placeholder="Buscar"
              onChange={this.handlebusqueda}
              id="buscar"
            />
          </Col>

          <ButtonGroup aria-label="Tercer group">
            <Button
              className="mr-2 "
              variant="primary"
              type="button"
              onClick={this.modalShowRegistro}
            >
              Agregar
            </Button>

            {/*
            <Button     className="mr-2" variant="primary" type="button" onClick={this.pdf}>
              PDF
            </Button>
          */}

            <ReactHTMLTableToExcel
              id="table"
              className="btn btn-success mr-2"
              table="table-reunion"
              filename="Reunion"
              sheet="Reunion"
              buttonText="Excel"
            />
          </ButtonGroup>

          <Col xs={0} sm={2}></Col>
        </Row>

        <br />
        <Row>
          <Col xs={0} sm={0}></Col>
          <Col xs={12} sm={12}>
            <Table
              responsive
              striped
              bordered
              hover
              variant="dark"
              id="table-reunion"
            >
              <thead>
                <tr>
                  <th>Acciones</th>
                  <th>Área</th>
                  <th>Identificación</th>
                  <th>Nombre</th>
                  <th>Apellidos</th>
                  <th>Edad</th>
                  <th>Celular</th>
                  <th>Fecha</th>
                  <th>Líder</th>
                  <th>Reunión</th>
                  <th>Contacto</th>
                  <th>Visita</th>
                  <th>Síntomas</th>
                  <th>Temperatura</th>
                  <th>Pregunta</th>
                </tr>
              </thead>
              {this.state.lista.map((e) => {
                return (
                  <tbody key={e.key}>
                    <tr>
                      <td>
                        <ButtonGroup vertical>
                          <Button
                            className=" btn btn-warning btn-sm mb-2"
                            onClick={() => this.llenar(e.key)}
                          >
                            Actualizar
                          </Button>

                          <Button
                            className=" btn btn-danger btn-sm"
                            onClick={() => this.confirm(e.key)}
                          >
                            Eliminar
                          </Button>
                        </ButtonGroup>
                      </td>
                      <td>{e.val().area}</td>
                      <td>{e.val().identificacion}</td>
                      <td>{e.val().nombre}</td>
                      <td>{e.val().apellido}</td>
                      <td>{e.val().edad}</td>
                      <td>{e.val().celular}</td>
                      <td>{e.val().fecha}</td>
                      <td>{e.val().lider.nombre}</td>
                      <td>{e.val().lider.reunion}</td>
                      <td>{e.val().contacto}</td>
                      <td>{e.val().visita}</td>
                      <td>{e.val().sintomas}</td>
                      <td>{e.val().temperatura}</td>
                      <td>{e.val().pregunta_covid}</td>
                    </tr>
                  </tbody>
                );
              })}
            </Table>
          </Col>
          <Col xs={0} sm={0}></Col>
        </Row>

        <Modal
          show={this.state.showModalActualizar}
          onHide={this.modalCloseActualizar}
        >
          <Modal.Header closeButton>
            <Modal.Title> Información Personal</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className={this.props.classes.paperCheckout}>
              <Grid container spacing={3}>
                <Grid item xs={12} sm={12}>
                  <TextField
                    required
                    id="temperatura"
                    name="temperatura"
                    label="Temperatura"
                    fullWidth
                    type="number"
                    autoComplete="family-temperatura"
                    onChange={this.handleInput}
                    value={this.state.temperatura}
                  />
                </Grid>
                <Grid item xs={12} md={12}>
                  <Select
                    required
                    id="pregunta_covid"
                    name="pregunta_covid"
                    label="¿Has presentado síntomas gripales en las últimas 8 horas?"
                    fullWidth
                    autoComplete="cc-sintomas"
                    onChange={this.handleInput}
                    value={this.state.pregunta_covid}
                  >
                    <MenuItem value="Si">Si</MenuItem>
                    <MenuItem value="No">No</MenuItem>
                  </Select>
                  <FormHelperText>
                    ¿Has presentado síntomas gripales en las últimas 8 horas?
                  </FormHelperText>
                </Grid>
                {errors2.campos_obligatorios && (
                  <Alert variant="danger">{errors2.campos_obligatorios} </Alert>
                )}
              </Grid>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button
              className="btn btn-danger"
              onClick={this.modalCloseActualizar}
            >
              Cancelar
            </Button>
            <Button onClick={this.handleupdate}>Guardar Cambios</Button>
          </Modal.Footer>
        </Modal>

        <Modal
          show={this.state.showModalRegistro}
          onHide={this.modalCloseRegistro}
        >
          <Modal.Header closeButton>
            <Modal.Title> Información Personal</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Grid container spacing={2}>
              <Grid item xs={12} md={12}>
                <Select
                  required
                  id="area"
                  name="area"
                  label=" ¿Donde te diriges?"
                  fullWidth
                  autoComplete="cc-area"
                  onChange={this.handleInput}
                  value={this.state.area}
                >
                  <MenuItem value="Auditorio">Auditorio</MenuItem>
                  <MenuItem value="Producción">Producción</MenuItem>
                  <MenuItem value="Alabaza">Alabaza</MenuItem>
                  <MenuItem value="Kids">Kids</MenuItem>
                  <MenuItem value="Servicio">Servicio</MenuItem>
                </Select>
                <FormHelperText>¿Donde te diriges?</FormHelperText>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="nombre"
                  name="nombre"
                  label="Nombre"
                  fullWidth
                  autoComplete="given-name"
                  onChange={this.handleInput}
                  inputProps={{
                    maxLength: 20,
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="apellido"
                  name="apellido"
                  label="Apellidos"
                  fullWidth
                  autoComplete="family-apellido"
                  onChange={this.handleInput}
                  inputProps={{
                    maxLength: 20,
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="edad"
                  name="edad"
                  label="Edad"
                  type="number"
                  fullWidth
                  autoComplete="family-edad"
                  onChange={this.handleInput}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="identificacion"
                  name="identificacion"
                  label="Identificación"
                  fullWidth
                  autoComplete="family-identificacion"
                  onChange={this.handleInput}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="celular"
                  name="celular"
                  label="Celular"
                  fullWidth
                  autoComplete="Family-celular"
                  onChange={this.handleInput}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <DatePicker
                  selected={this.state.fecha}
                  isClearable
                  locale="es"
                  dateFormat="dd 'de' MMMM 'de' yyyy"
                  filterDate={this.filtroFecha}
                  placeholderText="Seleccione fecha"
                  onChange={this.handleFecha}
                />
              </Grid>

              <Grid item xs={12}>
                <InputLabel htmlFor="lider">Líder</InputLabel>
                <Select
                  required
                  label="Líder de 12"
                  name="lider"
                  fullWidth
                  onChange={this.handleInput}
                  value={this.state.lider}
                >
                  {this.state.lista2.map((e) => {
                    return (
                      <MenuItem key={e.key} value={JSON.stringify(e.val())}>
                        {e.val().nombre}
                      </MenuItem>
                    );
                  })}
                </Select>
                {this.state.nombrelider}

                <FormHelperText>¿Quién es tu líder de 12?</FormHelperText>
              </Grid>
            </Grid>

            <br></br>
            <br></br>
            <br></br>

            <Typography variant="h6" gutterBottom>
              Información ante el Covid-19
            </Typography>
            <Grid container spacing={3}>
              <Grid item xs={12} md={12}>
                <Select
                  required
                  id="contacto"
                  name="contacto"
                  label="Has tenido contacto con personas que tengan covid-19?"
                  fullWidth
                  autoComplete="cc-contacto"
                  onChange={this.handleInput}
                  value={this.state.contacto}
                >
                  <MenuItem value="Si">Si</MenuItem>
                  <MenuItem value="No">No</MenuItem>
                </Select>
                <FormHelperText>
                  ¿Has tenido contacto con personas que tengan covid-19?
                </FormHelperText>
              </Grid>

              <Grid item xs={12} md={12}>
                <Select
                  required
                  id="visita"
                  name="visita"
                  label="¿Has visitado una clínica en los últimos 8 días?"
                  fullWidth
                  autoComplete="cc-visita"
                  onChange={this.handleInput}
                  value={this.state.visita}
                >
                  <MenuItem value="Si">Si</MenuItem>
                  <MenuItem value="No">No</MenuItem>
                </Select>
                <FormHelperText>
                  ¿Has visitado una clínica en los últimos 8 días?
                </FormHelperText>
              </Grid>

              <Grid item xs={12} md={12}>
                <Select
                  required
                  id="sintomas"
                  name="sintomas"
                  label="¿Has presentado síntomas gripales en las últimas 8 horas?"
                  fullWidth
                  autoComplete="cc-sintomas"
                  onChange={this.handleInput}
                  value={this.state.sintomas}
                >
                  <MenuItem value="Si">Si</MenuItem>
                  <MenuItem value="No">No</MenuItem>
                </Select>
                <FormHelperText>
                  ¿Has presentado síntomas gripales en las últimas 8 horas?
                </FormHelperText>
              </Grid>
              {errors.campos_obligatorios && (
                <Alert variant="danger">{errors.campos_obligatorios} </Alert>
              )}
            </Grid>
          </Modal.Body>
          <Modal.Footer>
            <Button
              className="btn btn-danger"
              onClick={this.modalCloseRegistro}
            >
              Cancelar
            </Button>
            <Button onClick={this.handleinsert}>Registrar</Button>
          </Modal.Footer>
        </Modal>

        <br></br>
      </div>
    );
  }
}

export default Styles(ReunionSabado);

import React from "react";
import {
  createMuiTheme,
  ThemeProvider,
  withStyles,
} from "@material-ui/core/styles";
import { Spinner } from "react-bootstrap";
import CssBaseline from "@material-ui/core/CssBaseline";
import Hidden from "@material-ui/core/Hidden";
import Copyright from "../Utilitys/Copyright";
import Navegator from "./Navegator";
import Content from "./Content";
import Header from "./Header";
import firebase from "firebase/app";
import "firebase/auth";

import { firebaseConfig } from "../Firebase";

let theme = createMuiTheme({
  palette: {
    primary: {
      light: "#63ccff",
      main: "#009be5",
      dark: "#006db3",
    },
  },
  typography: {
    h5: {
      fontWeight: 500,
      fontSize: 26,
      letterSpacing: 0.5,
    },
  },
  shape: {
    borderRadius: 8,
  },
  props: {
    MuiTab: {
      disableRipple: true,
    },
  },
  mixins: {
    toolbar: {
      minHeight: 48,
    },
  },
});

theme = {
  ...theme,
  overrides: {
    MuiDrawer: {
      paper: {
        backgroundColor: "#18202c",
      },
    },
    MuiButton: {
      label: {
        textTransform: "none",
      },
      contained: {
        boxShadow: "none",
        "&:active": {
          boxShadow: "none",
        },
      },
    },
    MuiTabs: {
      root: {
        marginLeft: theme.spacing(1),
      },
      indicator: {
        height: 3,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        backgroundColor: theme.palette.common.white,
      },
    },
    MuiTab: {
      root: {
        textTransform: "none",
        margin: "0 16px",
        minWidth: 0,
        padding: 0,
        [theme.breakpoints.up("md")]: {
          padding: 0,
          minWidth: 0,
        },
      },
    },
    MuiIconButton: {
      root: {
        padding: theme.spacing(1),
      },
    },
    MuiTooltip: {
      tooltip: {
        borderRadius: 4,
      },
    },
    MuiDivider: {
      root: {
        backgroundColor: "#404854",
      },
    },
    MuiListItemText: {
      primary: {
        fontWeight: theme.typography.fontWeightMedium,
      },
    },
    MuiListItemIcon: {
      root: {
        color: "inherit",
        marginRight: 0,
        "& svg": {
          fontSize: 20,
        },
      },
    },
    MuiAvatar: {
      root: {
        width: 32,
        height: 32,
      },
    },
  },
};

const drawerWidth = 250;

const styles = {
  root: {
    display: "flex",
    minHeight: "100vh",
    margin: "0px",
    width: "100%",
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  app: {
    flex: 1,
    width: drawerWidth,
    display: "flex",
    flexDirection: "column",
  },
  main: {
    flex: 1,

    background: "#eaeff1",
    padding: "3% 3% 0% 3%",

    marginLeft: "10px",
  },
  footer: {
    padding: theme.spacing(2),
    background: "#eaeff1",
  },
};

class Base extends React.Component {
  constructor() {
    super();

    this.state = { mobileOpen: false, ingreso: false };
  }
  componentDidMount() {
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
    firebase.auth().onAuthStateChanged((user) => {
      if (user != null) {
        console.log(user);
        this.setState({ ingreso: true });
      } else {
        window.location.href = "./iniciosesion";
      }
    });
  }

  handleDrawerToggle = () => {
    if (this.state.mobileOpen === false) {
      this.setState({ mobileOpen: true });
    }
    if (this.state.mobileOpen === true) {
      this.setState({ mobileOpen: false });
    }
  };

  render() {
    if (this.state.ingreso === true) {
      return (
        <div>
          <ThemeProvider theme={theme}>
            <div className={this.props.classes.root}>
              <CssBaseline />
              <nav className={this.props.classes.drawer}>
                <Hidden smUp implementation="js">
                  <Navegator
                    PaperProps={{ style: { width: drawerWidth } }}
                    variant="temporary"
                    open={this.state.mobileOpen}
                    onClose={this.handleDrawerToggle}
                  />
                </Hidden>
                <Hidden xsDown implementation="css">
                  <Navegator PaperProps={{ style: { width: drawerWidth } }} />
                </Hidden>
              </nav>
              <div className={this.props.classes.app}>
                <Header onDrawerToggle={this.handleDrawerToggle} />
                <main className={this.props.classes.main}>
                  <Content />
                </main>
                <footer className={this.props.classes.footer}>
                  <Copyright />
                </footer>
              </div>
            </div>
          </ThemeProvider>
        </div>
      );
    } else {
      return (
        <div>
          <div style={{ margin: "250px 0% " }}>
            <h1>Cargando</h1>
            <Spinner animation="grow" variant="secondary" role="status">
              <span className="sr-only">Loading...</span>
            </Spinner>
          </div>
        </div>
      );
    }
  }
}
export default withStyles(styles)(Base);

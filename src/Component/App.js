import React from "react";
import "../App.css";
import Inicio from "./Inicio";
import InicioSesion from "./Iniciosesion";
import Registro from "./Registro";
import Ingreso from "./Ingreso";
import ValidarFormularioSabado from "./ValidarFormularioSabado";
import ValidarFormularioDomingo from "./ValidarFormularioDomingo";
import PdfReunion from "./PdfReunion";
import firebase from "firebase/app";
import { firebaseConfig } from "../Firebase";
import "firebase/database";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

class App extends React.Component {
  componentDidMount() {
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
  }

  render() {
    return (
      <div className="App">
        <Router>
        <Switch>
          <Route exact path="/" component={Inicio}></Route>
          <Route path="/iniciosesion" component={InicioSesion}></Route>
          <Route path="/registro" component={Registro}></Route>
          <Route path="/ingreso" component={Ingreso}></Route>
          <Route
            path="/formulariosabado"
            component={ValidarFormularioSabado}
          ></Route>
          <Route
            path="/formulariodomingo"
            component={ValidarFormularioDomingo}
          ></Route>
          <Route path="/pdfReunion" component={PdfReunion}></Route>
        </Switch>
        </Router>
      </div>
    );
  }
}

export default App;

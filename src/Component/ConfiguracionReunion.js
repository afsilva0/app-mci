import React from "react";
import {
  Table,
  Button,
  Form,
  Row,
  Col,
  ButtonGroup,
  Modal,
  Alert,
} from "react-bootstrap";
import firebase from "firebase/app";
import "firebase/database";
import Grid from "@material-ui/core/Grid";
import DatePicker, { registerLocale } from "react-datepicker";
import ReactHTMLTableToExcel from "react-html-table-to-excel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import Select from "@material-ui/core/Select";
import es from "date-fns/locale/es";

registerLocale("es", es);

const validate = (values) => {
  const errors = {};

  if (!values.fechaInicioTime || !values.fechaFinalTime) {
    errors.campos_obligatorios = "Todos los campos son obligatorios";
  }

  if (values.fechaInicioTime > values.fechaFinalTime) {
    errors.campos_obligatorios =
      "La fecha final debe ser despues de la inicial";
  }

  return errors;
};

class ConfiguracionReunion extends React.Component {
  constructor() {
    super();

    this.state = {
      id: "",
      reunion: "Sabado",
      fechaInicio: "",
      fechaInicioFormato: "",
      fechaInicioTime: "",
      fechaFinal: "",
      fechaFinalFormato: "",
      fechaFinalTime: "",
      show: false,
      busqueda: "",
      lista: [],
      errors: {},
    };
  }

  cargarConfiguracion = () => {
    // Carga todo los datos de padre configuracion-reunion y los llena en lista
    this.setState({ lista: [] });
    firebase.database().ref("configuracion-reunion/").off();
    firebase
      .database()
      .ref("configuracion-reunion/")
      .on("child_added", (data) => {
        var listaTemp = this.state.lista;
        listaTemp.push(data);
        this.setState({ lista: listaTemp });
      });
  };

  componentDidMount() {
    this.cargarConfiguracion();
  }

  handleInput = async (e) => {
    const { value, name } = e.target;
    await this.setState({ [name]: value });
  };

  handleClose = () => {
    this.setState({ show: false });
    this.limpiarsestados();
  };
  handleShow = () => {
    this.setState({ show: true });
  };

  handlebusqueda = async (e) => {
    // obtiene la informacion de los campos de texto y los set en os estados
    await this.setState({ busqueda: e.target.value });
    this.filtroBusqueda();
  };

  limpiarsestados = () => {
    this.setState({ id: "" });
    this.setState({ fechaInicio: "" });
    this.setState({ fechaInicioFormato: "" });
    this.setState({ fechaInicioTime: "" });
    this.setState({ fechaFinal: "" });
    this.setState({ fechaFinalFormato: "" });
    this.setState({ fechaFinalTime: "" });
  };

  hora = (h) => {
    var hh;
    if (h === 0) {
      hh = "12";
    } else if (h > 0 && h <= 12) {
      hh = h;
    } else {
      for (var i = 0; i < 23; i++) {
        if (h === i) {
          hh = i - 12;
        }
      }
    }

    return hh;
  };

  horario = (a) => {
    var aa;
    if (a === 0) {
      aa = "AM";
    }
    if (a >= 12) {
      aa = "PM";
    } else {
      aa = "AM";
    }
    return aa;
  };

  minutos = (m) => {
    var mm;
    for (var i = 0; i <= 9; i++) {
      if (m === i) {
        mm = "0" + i;
      }
    }

    if (m > 9) {
      mm = m;
    }

    return mm;
  };

  handleFechaInicio = async (e) => {
    var fecha = `${e.getDate()}/${
      1 + e.getMonth()
    }/${e.getFullYear()} ${this.hora(e.getHours())}:${this.minutos(
      e.getMinutes()
    )} ${this.horario(e.getHours())}`;

    var fechaTime = e.getTime();

    await this.setState({ fechaInicio: e });
    await this.setState({ fechaInicioFormato: fecha });
    await this.setState({ fechaInicioTime: fechaTime });
  };

  handleFechaFinal = async (e) => {
    var fecha = `${e.getDate()}/${
      1 + e.getMonth()
    }/${e.getFullYear()} ${this.hora(e.getHours())}:${this.minutos(
      e.getMinutes()
    )} ${this.horario(e.getHours())}`;

    var fechaTime = e.getTime();

    await this.setState({ fechaFinal: e });
    await this.setState({ fechaFinalFormato: fecha });
    await this.setState({ fechaFinalTime: fechaTime });
  };

  handleinsert = (e) => {
    const { errors, ...sinErrors } = this.state;
    const result = validate(sinErrors);

    this.setState({ errors: result });

    //Esta linea de codigo valida si no existe errores y hace el registro
    if (!Object.keys(result).length) {
      var reunion = this.state.reunion;
      var fechaInicioFormato = this.state.fechaInicioFormato;
      var fechaFinalFormato = this.state.fechaFinalFormato;
      var fechaInicioTime = this.state.fechaInicioTime;
      var fechaFinalTime = this.state.fechaFinalTime;

      var objeto = {
        reunion: "",
        fechaInicioFormato: "",
        fechaInicioTime: "",
        fechaFinalFormato: "",
        fechaFinalTime: "",
      };
      objeto.reunion = reunion;
      objeto.fechaInicioFormato = fechaInicioFormato;
      objeto.fechaInicioTime = fechaInicioTime;
      objeto.fechaFinalFormato = fechaFinalFormato;
      objeto.fechaFinalTime = fechaFinalTime;

      firebase.database().ref("configuracion-reunion/").push(objeto);
      this.limpiarsestados();
      this.handleClose();
    }
  };

  remove = (id) => {
    var refe = "configuracion-reunion/";
    var referencia = refe + id;
    firebase.database().ref(referencia).remove();
    this.cargarConfiguracion();
  };

  filtroBusqueda = async () => {
    if (this.state.busqueda === "") {
      this.cargarConfiguracion();
    } else {
      var data = await firebase
        .database()
        .ref("configuracion-reunion/")
        .orderByChild("reunion")
        .equalTo(this.state.busqueda)
        .once("value");

      if (data.val() == null) {
        // this.cargarReuniones();
      } else {
        data.forEach((lista) => {
          if (lista.val().reunion === this.state.busqueda) {
            var listaTemp = [];
            listaTemp.push(lista);
            this.setState({ lista: listaTemp });
          }
        });
      }
    }
  };

  render() {
    const { errors } = this.state;
    return (
      <div>
        <h1>Configuración Reunión</h1>
        <Row>
          <Col xs={6} sm={6}>
            <Form.Control
              type="text"
              placeholder="Buscar"
              onChange={this.handlebusqueda}
              id="buscar"
            />
          </Col>

          <ButtonGroup aria-label="Tercer group">
            <Button
              className="mr-2 "
              variant="primary"
              type="button"
              onClick={this.handleShow}
            >
              Agregar
            </Button>
            <ReactHTMLTableToExcel
              id="table"
              className="btn btn-success mr-2"
              table="table-configuracion-reunion"
              filename="Reunion"
              sheet="Reunion"
              buttonText="Excel"
            />
          </ButtonGroup>

          <Col xs={0} sm={2}></Col>
        </Row>

        <br />
        <Table
          responsive
          striped
          bordered
          hover
          variant="dark"
          id="table-configuracion-reunion"
        >
          <thead>
            <tr>
              <th>Acciones</th>
              <th>Reunión </th>
              <th>Fecha Inicio</th>
              <th>Fecha Final</th>
            </tr>
          </thead>

          {this.state.lista.map((e) => {
            return (
              <tbody key={e.key}>
                <tr>
                  <td>
                    <ButtonGroup>
                      <Button
                        className=" btn btn-danger btn-sm"
                        onClick={() => this.remove(e.key)}
                      >
                        Eliminar
                      </Button>
                    </ButtonGroup>
                  </td>
                  <td>{e.val().reunion}</td>
                  <td>{e.val().fechaInicioFormato}</td>
                  <td>{e.val().fechaFinalFormato}</td>
                </tr>
              </tbody>
            );
          })}
        </Table>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title> Configuración Reunión</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Grid container spacing={2}>
              <Grid item xs={12} md={12}>
                <Select
                  required
                  id="reunion"
                  name="reunion"
                  label="Reunión"
                  fullWidth
                  autoComplete="cc-reunion"
                  onChange={this.handleInput}
                  value={this.state.reunion}
                >
                  <MenuItem value="Sabado">Sábado</MenuItem>
                  <MenuItem value="Domingo">Domingo</MenuItem>
                </Select>
                <FormHelperText>Reunión</FormHelperText>
              </Grid>
              <Grid item xs={12} sm={6}>
                <DatePicker
                  selected={this.state.fechaInicio}
                  onChange={this.handleFechaInicio}
                  timeInputLabel="Time:"
                  dateFormat="dd/MM/yyyy h:mm aa"
                  showTimeInput
                  locale="es"
                  placeholderText="Ingrese la fecha Inicial"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <DatePicker
                  selected={this.state.fechaFinal}
                  onChange={this.handleFechaFinal}
                  timeInputLabel="Time:"
                  dateFormat="dd/MM/yyyy h:mm aa"
                  showTimeInput
                  locale="es"
                  placeholderText="Ingrese la fecha final"
                />
              </Grid>
            </Grid>
            <br></br>
            {errors.campos_obligatorios && (
              <Alert variant="danger">{errors.campos_obligatorios} </Alert>
            )}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Cancelar
            </Button>
            <Button variant="primary" onClick={this.handleinsert}>
              Registrar
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ConfiguracionReunion;

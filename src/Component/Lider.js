import React from "react";
import {
  Table,
  Button,
  Form,
  Row,
  Col,
  ButtonGroup,
  Modal,
  Alert,
} from "react-bootstrap";
import firebase from "firebase/app";
import "firebase/database";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import Select from "@material-ui/core/Select";
import ReactHTMLTableToExcel from "react-html-table-to-excel";

const validate = (values) => {
  const errors = {};

  if (
    !values.identificacion ||
    !values.nombre ||
    !values.apellido ||
    !values.celular ||
    !values.correo ||
    !values.reunion
  ) {
    errors.campos_obligatorios = "Todos los campos son obligatorios";
  }

  return errors;
};

const validate2 = (values) => {
  const errors2 = {};

  if (
    !values.identificacion ||
    !values.nombre ||
    !values.apellido ||
    !values.celular ||
    !values.correo ||
    !values.reunion
  ) {
    errors2.campos_obligatorios = "Todos los campos son obligatorios";
  }

  return errors2;
};

class Lider extends React.Component {
  constructor() {
    super();

    this.state = {
      id: "",
      identificacion: "",
      nombre: "",
      apellido: "",
      celular: "",
      correo: "",
      reunion: "",
      lista: [],
      busqueda: "",
      errors: {},
      errors2: {},

      showModalRegistro: false,
      showModalActualizar: false,
    };
  }

  cargarLideres = () => {
    // Carga todo los datos de padre lider y los llena en lista
    this.setState({ lista: [] });
    firebase.database().ref("lider/").off();
    firebase
      .database()
      .ref("lider/")
      .on("child_added", (data) => {
        var listaTemp = this.state.lista;
        listaTemp.push(data);
        this.setState({ lista: listaTemp });
      });
  };
  componentDidMount() {
    this.cargarLideres();
  }

  modalCloseRegistro = () => {
    this.setState({ showModalRegistro: false });
  };

  modalShowRegistro = () => {
    this.setState({ showModalRegistro: true });
  };

  modalCloseActualizar = () => {
    this.setState({ showModalActualizar: false });
  };
  modalShowActualizar = () => {
    this.setState({ showModalActualizar: true });
  };

  limpiarsestados = () => {
    this.setState({ id: "" });
    this.setState({ identificacion: "" });
    this.setState({ nombre: "" });
    this.setState({ apellido: "" });
    this.setState({ celular: "" });
    this.setState({ correo: "" });
    this.setState({ reunion: "" });
  };

  handleInput = async (e) => {
    // obtiene la informacion de los campos de texto y los set en os estados
    const { value, name } = e.target;
    await this.setState({ [name]: value });
  };

  handlebusqueda = async (e) => {
    // obtiene la informacion del campo de busqueda y lo set en el estado y ejecuta el filtro de busquedad
    await this.setState({ busqueda: e.target.value });
    this.filtroBusqueda();
  };

  handleinsert = (e) => {
    const { errors, ...sinErrors } = this.state;
    const result = validate(sinErrors);

    this.setState({ errors: result });
    if (!Object.keys(result).length) {
      var objeto = {
        identificacion: "",
        nombre: "",
        apellido: "",
        celular: "",
        correo: "",
        reunion: "",
      };
      objeto.identificacion = this.state.identificacion;
      objeto.nombre = this.state.nombre;
      objeto.apellido = this.state.apellido;
      objeto.celular = this.state.celular;
      objeto.correo = this.state.correo;
      objeto.reunion = this.state.reunion;
      firebase.database().ref("lider/").push(objeto);
      this.limpiarsestados();
      this.modalCloseRegistro();
    }
  };

  remove = (id) => {
    firebase
      .database()
      .ref("lider/" + id)
      .remove();
    this.cargarLideres();
  };

  llenar = (id, identificacion, nombre, apellido, celular, correo, reunion) => {
    this.setState({ id: id });
    this.setState({ identificacion: identificacion });
    this.setState({ nombre: nombre });
    this.setState({ apellido: apellido });
    this.setState({ celular: celular });
    this.setState({ correo: correo });
    this.setState({ reunion: reunion });

    this.modalShowActualizar();
  };

  handleupdate = () => {
    const { errors2, ...sinErrors2 } = this.state;
    const result2 = validate2(sinErrors2);

    this.setState({ errors2: result2 });
    if (!Object.keys(result2).length) {
      var objeto = {
        identificacion: "",
        nombre: "",
        apellido: "",
        celular: "",
        correo: "",
        reunion: "",
      };
      objeto.identificacion = this.state.identificacion;
      objeto.nombre = this.state.nombre;
      objeto.apellido = this.state.apellido;
      objeto.celular = this.state.celular;
      objeto.correo = this.state.correo;
      objeto.reunion = this.state.reunion;
      firebase
        .database()
        .ref("lider/" + this.state.id)
        .update(objeto);
      this.limpiarsestados();
      this.cargarLideres();
      this.modalCloseActualizar();
    }
  };

  filtroBusqueda = async () => {
    if (this.state.busqueda === "") {
      this.cargarLideres();
    } else {
      var data = await firebase
        .database()
        .ref("lider/")
        .orderByChild("identificacion")
        .equalTo(this.state.busqueda)
        .once("value");

      if (data.val() == null) {
        //this.cargarLideres();
      } else {
        data.forEach((lista) => {
          if (lista.val().identificacion === this.state.busqueda) {
            var listaTemp = [];
            listaTemp.push(lista);
            this.setState({ lista: listaTemp });
          }
        });
      }
    }
  };

  render() {
    const { errors } = this.state;
    const { errors2 } = this.state;
    return (
      <div>
        <h1>Líder de 12</h1>
        <Row>
          <Col xs={6} sm={6}>
            <Form.Control
              type="text"
              placeholder="Buscar"
              name="busqueda"
              onChange={this.handlebusqueda}
            />
          </Col>

          <ButtonGroup aria-label="Tercer group">
            <Button
              className="mr-2 "
              variant="primary"
              type="button"
              onClick={this.modalShowRegistro}
            >
              Agregar
            </Button>
            <ReactHTMLTableToExcel
              id="table"
              className="btn btn-success mr-2"
              table="table-lider"
              filename="Lider"
              sheet="Lider"
              buttonText="Excel"
            />
          </ButtonGroup>
        </Row>

        <br />
        <Table
          responsive
          striped
          bordered
          hover
          variant="dark"
          id="table-lider"
        >
          <thead>
            <tr>
              <th>Acciones</th>
              <th>Identificación</th>
              <th>Nombre</th>
              <th>Apellidos</th>
              <th>Celular</th>
              <th>Correo</th>
              <th>Reunión</th>
            </tr>
          </thead>
          {this.state.lista.map((e) => {
            return (
              <tbody key={e.key}>
                <tr>
                  <td>
                    <ButtonGroup vertical>
                      <Button
                        type="button"
                        className=" btn btn-warning btn-sm mb-2"
                        onClick={() =>
                          this.llenar(
                            e.key,
                            e.val().identificacion,
                            e.val().nombre,
                            e.val().apellido,
                            e.val().celular,
                            e.val().correo,
                            e.val().reunion
                          )
                        }
                        size="sm"
                      >
                        Actualizar
                      </Button>

                      <Button
                        type="button"
                        className=" btn btn-danger btn-sm"
                        onClick={() => this.remove(e.key)}
                        size="sm"
                      >
                        Eliminar
                      </Button>
                    </ButtonGroup>
                  </td>
                  <td>{e.val().identificacion}</td>
                  <td>{e.val().nombre}</td>
                  <td>{e.val().apellido}</td>
                  <td>{e.val().celular}</td>
                  <td>{e.val().correo}</td>
                  <td>{e.val().reunion}</td>
                </tr>
              </tbody>
            );
          })}
        </Table>

        <Modal
          show={this.state.showModalActualizar}
          onHide={this.modalCloseActualizar}
        >
          <Modal.Header closeButton>
            <Modal.Title> Información Personal</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={12}>
                <TextField
                  required
                  id="identificacion"
                  name="identificacion"
                  label="Identificación"
                  fullWidth
                  autoComplete="family-identificacion"
                  onChange={this.handleInput}
                  value={this.state.identificacion}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="nombre"
                  name="nombre"
                  label="Nombre"
                  fullWidth
                  autoComplete="given-name"
                  onChange={this.handleInput}
                  value={this.state.nombre}
                  inputProps={{
                    maxLength: 20,
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="apellido"
                  name="apellido"
                  label="Apellidos"
                  fullWidth
                  autoComplete="family-name"
                  onChange={this.handleInput}
                  value={this.state.apellido}
                  inputProps={{
                    maxLength: 20,
                  }}
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="celular"
                  name="celular"
                  label="Celular"
                  fullWidth
                  autoComplete="Family-celular"
                  onChange={this.handleInput}
                  value={this.state.celular}
                  inputProps={{
                    maxLength: 15,
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="correo"
                  name="correo"
                  label="Correo"
                  fullWidth
                  autoComplete="Family-correo"
                  onChange={this.handleInput}
                  value={this.state.correo}
                  inputProps={{
                    maxLength: 30,
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <Select
                  required
                  label="Reunión"
                  name="reunion"
                  id="reunion"
                  onChange={this.handleInput}
                  fullWidth
                  value={this.state.reunion}
                >
                  <MenuItem value="Reunión 1">Reunión 1</MenuItem>
                  <MenuItem value="Reunión 2">Reunión 2</MenuItem>
                  <MenuItem value="Reunión 3">Reunión 3</MenuItem>
                </Select>
                <FormHelperText>¿Cuál es tu reunión?</FormHelperText>
              </Grid>

              {errors2.campos_obligatorios && (
                <Alert variant="danger">{errors2.campos_obligatorios} </Alert>
              )}
            </Grid>
          </Modal.Body>
          <Modal.Footer>
            <Button
              className="btn btn-danger"
              onClick={this.modalCloseActualizar}
            >
              Cancelar
            </Button>
            <Button type="button" onClick={this.handleupdate}>
              Guardar Cambios
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal
          show={this.state.showModalRegistro}
          onHide={this.modalCloseRegistro}
        >
          <Modal.Header closeButton>
            <Modal.Title> Información Personal</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={12}>
                <TextField
                  required
                  id="identificacion"
                  name="identificacion"
                  label="Identificación"
                  fullWidth
                  autoComplete="family-identificacion"
                  onChange={this.handleInput}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="nombre"
                  name="nombre"
                  label="Nombre"
                  fullWidth
                  autoComplete="given-name"
                  onChange={this.handleInput}
                  inputProps={{
                    maxLength: 20,
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="apellido"
                  name="apellido"
                  label="Apellidos"
                  fullWidth
                  autoComplete="family-name"
                  onChange={this.handleInput}
                  inputProps={{
                    maxLength: 20,
                  }}
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="celular"
                  name="celular"
                  label="Celular"
                  fullWidth
                  autoComplete="Family-celular"
                  onChange={this.handleInput}
                  inputProps={{
                    maxLength: 15,
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="correo"
                  name="correo"
                  label="Correo"
                  fullWidth
                  autoComplete="Family-correo"
                  onChange={this.handleInput}
                  inputProps={{
                    maxLength: 30,
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <Select
                  required
                  label="Reunión"
                  name="reunion"
                  id="reunion"
                  onChange={this.handleInput}
                  value={this.state.reunion}
                  fullWidth
                >
                  <MenuItem value="Reunión 1">Reunión 1</MenuItem>
                  <MenuItem value="Reunión 2">Reunión 2</MenuItem>
                  <MenuItem value="Reunión 3">Reunión 3</MenuItem>
                </Select>
                <FormHelperText>¿Cuál es tu reunión?</FormHelperText>
              </Grid>
              {errors.campos_obligatorios && (
                <Alert variant="danger">{errors.campos_obligatorios} </Alert>
              )}
            </Grid>
          </Modal.Body>
          <Modal.Footer>
            <Button
              className="btn btn-danger"
              onClick={this.modalCloseRegistro}
            >
              Cancelar
            </Button>
            <Button onClick={this.handleinsert}>Registrar</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Lider;

import { createMuiTheme } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";

const useStyles = (theme) => ({
  paper: {
    marginTop: "15%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },

  papercontent: {
    maxWidth: 936,
    margin: "auto",
    overflow: "hidden",
  },

  paperModal: {
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`,
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },

  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  root: {
    border: 0,
    color: "white",
    height: 48,
    width: 600,
    padding: "0 100px",
  },

  background: {
    backgroundImage: `url("https://mci12.com/wp-content/uploads/2018/05/Banner-volviendo-al-original.jpg")`,

    display: "block",
    margin: "auto",
    width: "300%",
    maxWidth: "100%",

    height: "auto",

    color: theme.palette.common.white,
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
  },
  button: {
    minWidth: 200,
  },
  h5Inicio: {
    marginBottom: theme.spacing(4),
    marginTop: theme.spacing(4),
    padding: "0px  10% ",
    alignItems: "center",
    [theme.breakpoints.up("sm")]: {
      marginTop: theme.spacing(10),
    },
  },
  textInicio: {
    
    alignItems: "center",
  },

  more: {
    marginTop: theme.spacing(2),
  },

  appBarCheckout: {
    position: "relative",
  },
  layoutCheckout: {
    width: "auto",
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  paperCheckout: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  stepperCheckout: {
    padding: theme.spacing(3, 0, 5),
  },
  buttonsCheckout: {
    display: "flex",
    justifyContent: "flex-end",
  },
  buttonCheckout: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },

  imagenInformacion: {
    display: "block",
    margin: "auto",
    width: "100%",
    maxWidth: "100%",
    height: "auto",
    color: theme.palette.common.white,
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
  },
});

const theme = createMuiTheme({
  direction: "rtl",
  typography: {
    fontFamily: ["Roboto", "Helvetica", "Arial", "sans-serif"].join(","),
  },
  overrides: {
    MuiCssBaseline: {
      "@global": {},
    },
  },
});

export default withStyles(useStyles(theme));
